Endpoints : GET /bookings
            GET /partners
            POST /bookings

Mandatory data structure :
{
    "date": "date in format Y-m-d",
    "partner": {
        "name": "string"
    }
}
